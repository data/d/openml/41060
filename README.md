# OpenML dataset: London-smart-meter-data-preprocessed-for-clustering

https://www.openml.org/d/41060

**WARNING: This dataset is still in preparation.**

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Original data were acquired here:
https://www.kaggle.com/jeanmidev/smart-meters-in-london
Dataset contains smart meter data from London. Each row represents one consumer (together 5066 consumers). Dates are from 1.5.2013 - 30.9.2019 so together 7344 columns (period is 48).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/41060) of an [OpenML dataset](https://www.openml.org/d/41060). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/41060/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/41060/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/41060/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

